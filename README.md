# E3 IOC template

This repository contains a template for a standard ESS E3 IOC startup script.

## Modules

The startup script includes the standard ESS EPICS modules:
* iocStats
* recsync
* caPutLog
* ess
* autosave

Other modules can be added as required.

## Databases

Local databases for this IOC should be stored in the `db` directory. Instructions to load the database should be included in the startup script.

