# All require statments must have a single version number
## Edit version numbers as required
require iocstats, 3.1.16
#require s7plc
#require modbus
require streamdevice
require fug
###require autosave, 5.10.0
###require caPutLog, b544f92
###require recsync, 1.3.0-9705e52
###require ess, 0.0.1

## Add extra modules here

# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet("PREFIX", "PLC-01")
###epicsEnvSet("DEVICE", "EDIT: change this")
###epicsEnvSet("LOCATION", "EDIT: change this")
###epicsEnvSet("ENGINEER", "EDIT: Name <email@esss.se>"
###epicsEnvSet("IOC_NAME", "$(PREFIX)")

epicsEnvSet("IOC_NAME", "$(PREFIX)")






## Autosave environment variables
###epicsEnvSet("AS_TOP", "EDIT: Set autosave top level directory")
## Logging environment variables
###epicsEnvSet("GRAYLOG_SERVER_NAME", "EDIT: log1.tn.esss.lu.se")
###epicsEnvSet("ERRORLOG_SERVER_PORT", "9001")
###epicsEnvSet("CAPUTLOG_SERVER_PORT", "9001")
#epicsEnvSet("FACNAME", "EDIT: change this")
## Access security environment variables
###epicsEnvSet("PATH_TO_ASG_FILES", "$(ESS_module_dir)")
###epicsEnvSet("ASG_FILENAME", "unrestricted_access.asg")
## Add extra environment variables here

# Load standard module startup scripts
###iocshLoad("$(iocStats_DIR)/iocStats.iocsh", IOCNAME=$(IOC_NAME))
###iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(AS_TOP),IOCNAME=$(IOC_NAME),SEQ_PERIOD=60")
###iocshLoad("$(ess_DIR)/iocLog.iocsh", "LOG_INET=$(GRAYLOG_SERVER_NAME),LOG_INET_PORT=$(ERRORLOG_SERVER_PORT),FACNAME=$(FACILITY_NAME),IOCNAME=$(IOC_NAME)")
###iocshLoad("$(ess_DIR)/accessSecurityGroup.iocsh", "ASG_PATH=$(PATH_TO_ASG_FILES),ASG_FILE=$(ASG_FILENAME)")
###iocshLoad("$(caPutLog_DIR)/caPutLog.iocsh", "LOG_INET=$(GRAYLOG_SERVER_NAME),LOG_INET_PORT=$(CAPUTLOG_SERVER_PORT)")
###iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(IOC_NAME)")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")
#loadIocsh(isrc-010_ctrl-plc-01.cmd.iocsh, "IPADDR=172.30.5.47, RECVTIMEOUT=3000")
#loadIocsh("$(E3_IOCSH_TOP)/iocsh/isrc-010_ctrl-plc-01.cmd.iocsh", "IPADDR=172.30.5.47, RECVTIMEOUT=3000")





## Load custom databases
#cd $(TOP)
# dbLoadRecords("db/custom_database1.db", "MACRO1=MACRO1_VALUE,...")
# dbLoadTemplate("db/custom_database2.substitutions", "MACRO1=MACRO1_VALUE,...")


############################################################
######## FUG HCH 15k-100k [High Voltage Power Supply] ######
############################################################
#drvAsynIPPortConfigure("HVPS", "172.16.60.57:2101")
#drvAsynIPPortConfigure("HVPS", "127.0.0.1:9999")
#dbLoadRecords("fughch15k100k.db")
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=172.16.60.57, P=ISrc-010:, R=ISS-HVPS:")
iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=127.0.0.1, P=ISrc-010:, R=ISS-HVPS:")


############################################################
#################  iocStats  ###############################
############################################################

#dbLoadTemplate(iocAdminSoft.substitutions,IOC="ISrc-010:Ctrl-IOC-ISrc")
############################################################



# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

